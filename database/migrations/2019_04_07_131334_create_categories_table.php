<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('alias');
            $table->string('icon');
            $table->unsignedBigInteger('parent');
            $table->unsignedBigInteger('author');            
            $table->timestamps();
        });
        
        Schema::table('categories', function(Blueprint $table) {
            $table->foreign('author')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('parent')->references('id')->on('categories')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
