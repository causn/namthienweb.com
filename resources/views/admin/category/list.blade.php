@extends('admin.layout')
@section('title','Category')    
@section('content')
    <div class="col-md-12">
        <table id="categoryTable"  class="table table-bordered table-striped table-hover">
            <thead>
                 <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Author</th>                    
                    <th>Date</th>
                    <th>Action</th>
                 </tr>
            </thead>   
            <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $category->title }}</td>
                        <td>{{ $category->author }}</td>
                        <td>{{ $category->created_at }}</td>
                        <td>
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editModal">Edit</button>
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>         
        </table>    
        {{ $categories->links() }}    
    </div>    
    @include('admin.category.edit')
    @include('admin.category.delete')
@endsection
