@extends('layouts.app')

@section('content')
@section('scripts')
 <script type='text/javascript' src='{{ asset("themes/taurus/js/charts.js") }}'></script>
 <script type='text/javascript' src='{{ asset("themes/taurus/js/actions.js") }}'></script>
 <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/uniform/jquery.uniform.min.js") }}'></script>
 
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/jquery/jquery-ui.min.js") }}'></script>
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/jquery/jquery-migrate.min.js") }}'></script>
    <script type='text/javascript' src='{{ asset("themes/taurus/js/plugins/jquery/globalize.js") }}'></script>    
@endsection
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
