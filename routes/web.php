<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::namespace('Admin')->group(function () {
	Route::prefix('admin')->group(function () {
		Route::get('/', 'HomeController@index')->name('admin.home');
		Route::get('/category', 'CategoryController@listView')->name('admin.category');	
		Route::get('/user/filter','UserController@filter')->name('admin.user.filter');	
	});
});